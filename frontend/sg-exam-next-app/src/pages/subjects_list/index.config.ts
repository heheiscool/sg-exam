export default {
    navigationBarTitleText: '题库信息',
    enablePullDownRefresh: true,
    backgroundTextStyle: 'dark',
    usingComponents: {
        wxparse: '../../components/wxparse/index'
    }
} as Taro.PageConfig